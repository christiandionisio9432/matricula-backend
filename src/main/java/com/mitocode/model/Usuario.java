package com.mitocode.model;

import java.util.List;

import javax.validation.constraints.NotEmpty;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.fasterxml.jackson.annotation.JsonInclude;

@Document(collection = "usuarios")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Usuario {
	
	@Id
	private String id;
	
	@NotEmpty
	@Field(name = "usuario")
	private String usuario;
	
	@NotEmpty
	@Field(name = "clave")
	private String clave;
	
	@Field(name = "estado")
	private Boolean estado;
	
	private List<Rol> roles;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public Boolean getEstado() {
		return estado;
	}

	public void setEstado(Boolean estado) {
		this.estado = estado;
	}

	public List<Rol> getRoles() {
		return roles;
	}

	public void setRoles(List<Rol> roles) {
		this.roles = roles;
	}

}
