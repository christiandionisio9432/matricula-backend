package com.mitocode.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mitocode.model.Matricula;
import com.mitocode.pagination.PageSupport;
import com.mitocode.service.IMatriculaService;

import reactor.core.publisher.Mono;

@RestController
@RequestMapping("matriculas")
public class MatriculaController {

	@Autowired
	private IMatriculaService service;
	
	@PostMapping
	public Mono<ResponseEntity<Matricula>> registrar(@RequestBody Matricula matricula) {
		return service.registrar(matricula)
					.map(resp -> ResponseEntity.ok()
								.contentType(MediaType.APPLICATION_JSON)
								.body(resp))
					.defaultIfEmpty(new ResponseEntity<Matricula>(HttpStatus.NOT_FOUND));
	}
	
	@GetMapping("/pageable")
	public Mono<ResponseEntity<PageSupport<Matricula>>> listarPagebale(
			@RequestParam(name = "page", defaultValue = "0") int page,
		    @RequestParam(name = "size", defaultValue = "10") int size
			){
		
		Pageable pageRequest = PageRequest.of(page, size);
		
		return service.listarPage(pageRequest)
				.map(p -> ResponseEntity.ok()
						.contentType(MediaType.APPLICATION_JSON)
						.body(p)	
						)
				.defaultIfEmpty(ResponseEntity.noContent().build());
	
	}
}
