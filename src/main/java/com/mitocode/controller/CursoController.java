package com.mitocode.controller;

import java.net.URI;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mitocode.model.Curso;
import com.mitocode.pagination.PageSupport;
import com.mitocode.service.ICursoService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/cursos")
public class CursoController {
	
	@Autowired
	private ICursoService service;
	
	@GetMapping
	public Mono<ResponseEntity<Flux<Curso>>> listar() {
		Flux<Curso> fxCursos = service.listar();
		return Mono.just(ResponseEntity.ok()
						.contentType(MediaType.APPLICATION_JSON)
						.body(fxCursos));
	}
	
	@GetMapping("/{id}")
	public Mono<ResponseEntity<Curso>> listarPorId(@PathVariable("id") String id) {
		Mono<Curso> monoCurso = service.listarPorId(id);
		return monoCurso.map(respMono -> ResponseEntity.ok()
								.contentType(MediaType.APPLICATION_JSON)
								.body(respMono))
							.defaultIfEmpty(new ResponseEntity<Curso>(HttpStatus.NOT_FOUND));
	}
	
	@PostMapping
	public Mono<ResponseEntity<Curso>> registrar(@RequestBody @Valid Curso c, final ServerHttpRequest req){
		
		return service.registrar(c)
				.map(respMono -> ResponseEntity.created(URI.create(req.getURI().toString().concat("/").concat(c.getId())))
				.contentType(MediaType.APPLICATION_JSON)
				.body(respMono))
			.defaultIfEmpty(new ResponseEntity<Curso>(HttpStatus.NOT_FOUND));
	}
	
	@PutMapping("/{id}")
	public Mono<ResponseEntity<Curso>> modificar(@RequestBody @Valid Curso curso, @PathVariable("id") String id) {
		
		Mono<Curso> monoCurso = Mono.just(curso);
		Mono<Curso> clienteBD = service.listarPorId(id);
		
		return clienteBD.zipWith(monoCurso, (bd, c) -> {
						bd.setId(id);
						bd.setNombre(c.getNombre());
						bd.setSiglas(c.getSiglas());
						bd.setEstado(c.getEstado());
						return bd;
					})
					.flatMap(service::modificar)
					.map(resp -> ResponseEntity.ok()
									.contentType(MediaType.APPLICATION_JSON)
									.body(resp))
					.defaultIfEmpty(new ResponseEntity<Curso>(HttpStatus.NOT_FOUND));
	}
	
	@DeleteMapping("/{id}")
	public Mono<ResponseEntity<Void>> eliminar(@PathVariable("id") String id) {
		
		Mono<Curso> cursoBD = service.listarPorId(id);

		return cursoBD.flatMap(resp -> {
							return service.eliminar(id)
									.then(Mono.just(new ResponseEntity<Void>(HttpStatus.NO_CONTENT)));
							})
						.defaultIfEmpty(new ResponseEntity<Void>(HttpStatus.NOT_FOUND));
		
	}
	
	@GetMapping("/pageable")
	public Mono<ResponseEntity<PageSupport<Curso>>> listarPagebale(
			@RequestParam(name = "page", defaultValue = "0") int page,
		    @RequestParam(name = "size", defaultValue = "10") int size
			){
		
		Pageable pageRequest = PageRequest.of(page, size);
		
		return service.listarPage(pageRequest)
				.map(p -> ResponseEntity.ok()
						.contentType(MediaType.APPLICATION_JSON)
						.body(p)	
						)
				.defaultIfEmpty(ResponseEntity.noContent().build());
	
	}

}
