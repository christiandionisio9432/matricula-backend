package com.mitocode.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mitocode.model.Estudiante;
import com.mitocode.pagination.PageSupport;
import com.mitocode.service.IEstudianteService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/estudiantes")
public class EstudianteController {
	
	@Autowired
	private IEstudianteService service;
	
	
	@GetMapping
	public Mono<ResponseEntity<Flux<Estudiante>>> listar() {
		Flux<Estudiante> fxEstudiante = service.listar();
		
		return Mono.just(ResponseEntity.ok()
						.contentType(MediaType.APPLICATION_JSON)
						.body(fxEstudiante));
	}
	
	@GetMapping("/{id}")
	public Mono<ResponseEntity<Estudiante>> listarPorId(@PathVariable("id") String id) {
		Mono<Estudiante> monoEstudiante = service.listarPorId(id);
		return monoEstudiante.map(resp -> ResponseEntity.ok()
									.contentType(MediaType.APPLICATION_JSON)
									.body(resp))
							.defaultIfEmpty(new ResponseEntity<Estudiante>(HttpStatus.NOT_FOUND));
	}
	
	@PostMapping
	public Mono<ResponseEntity<Estudiante>> registrar(@RequestBody @Valid Estudiante estudiante) {
		return service.registrar(estudiante)
				.map(resp -> ResponseEntity.ok()
							.contentType(MediaType.APPLICATION_JSON)
							.body(resp))
				.defaultIfEmpty(new ResponseEntity<Estudiante>(HttpStatus.NOT_FOUND));
	}
	
	@PutMapping("/{id}")
	public Mono<ResponseEntity<Estudiante>> modificar(@RequestBody @Valid Estudiante estudiante, @PathVariable("id") String id) {
		Mono<Estudiante> monoEstudiante = Mono.just(estudiante);
		Mono<Estudiante> monoBD = service.listarPorId(id);
		
		return monoBD.zipWith(monoEstudiante, (bd, e) -> {
			bd.setNombres(e.getNombres());
			bd.setApellidos(e.getApellidos());
			bd.setDni(e.getDni());
			bd.setEdad(e.getEdad());
			return bd;
		})
		.flatMap(service::modificar)
		.map(resp -> ResponseEntity.ok()
					.contentType(MediaType.APPLICATION_JSON)
					.body(resp))
		.defaultIfEmpty(new ResponseEntity<Estudiante>(HttpStatus.NOT_FOUND));
	}
	
	@DeleteMapping("/{id}")
	public Mono<ResponseEntity<Void>> eliminar(@PathVariable("id") String id) {
		Mono<Estudiante> monoEstudiante = service.listarPorId(id);
		
		return monoEstudiante.flatMap(resp -> {
			return service.eliminar(id)
					.then(Mono.just(new ResponseEntity<Void>(HttpStatus.NO_CONTENT)));
		}).defaultIfEmpty(new ResponseEntity<Void>(HttpStatus.NOT_FOUND));
	}
	
	@GetMapping("/pageable")
	public Mono<ResponseEntity<PageSupport<Estudiante>>> listarPagebale(
			@RequestParam(name = "page", defaultValue = "0") int page,
		    @RequestParam(name = "size", defaultValue = "10") int size
			){
		
		Pageable pageRequest = PageRequest.of(page, size);
		
		return service.listarPage(pageRequest)
				.map(p -> ResponseEntity.ok()
						.contentType(MediaType.APPLICATION_JSON)
						.body(p)	
						)
				.defaultIfEmpty(ResponseEntity.noContent().build());
	
	}
	

}
